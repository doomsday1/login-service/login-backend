import os

# Hostname to serve
host = os.getenv("LOGIN_HOST", "0.0.0.0")

# Port to serve (inside docker)
port = int(os.getenv("LOGIN_PORT", 8080))

redirect_path = "/"
