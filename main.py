import os
from datetime import datetime, timedelta
from typing import Annotated, Optional

from fastapi import Depends, FastAPI, HTTPException, status, Form
from fastapi.security import OAuth2PasswordBearer, OAuth2PasswordRequestForm
from jose import JWTError, jwt
from passlib.context import CryptContext
from pydantic import BaseModel
from starlette.responses import Response, FileResponse
from starlette.staticfiles import StaticFiles

import config

SECRET_KEY = os.urandom(32)
ALGORITHM = "HS256"
ACCESS_TOKEN_EXPIRE_MINUTES = 30


class OAuth2ExtendedForm(OAuth2PasswordRequestForm):
    def __init__(self,
                 remember: bool | None = Form(None),
                 grant_type: str = Form(default=None, regex="password"),
                 username: str = Form(),
                 password: str = Form(),
                 scope: str = Form(default=""),
                 client_id: Optional[str] = Form(default=None),
                 client_secret: Optional[str] = Form(default=None)
                 ):
        super().__init__(grant_type=grant_type,
                         username=username,
                         password=password,
                         scope=scope,
                         client_id=client_id,
                         client_secret=client_secret)

        self.remember = remember


class Token(BaseModel):
    access_token: str
    token_type: str


class TokenData(BaseModel):
    username: str | None = None


class User(BaseModel):
    username: str
    email: str | None = None
    full_name: str | None = None
    disabled: bool | None = None


class UserInDB(User):
    hashed_password: str


fake_users_db = {
    "username": UserInDB(
        username="username",
        full_name="John Doe",
        email="johndoe@example.com",
        hashed_password="$2y$12$iqVd4p9.u28wOsSntJhHfetNN11mkzeUsA6Z8x5uSKFJ3rK9dw1yi",
        disabled=False,
    )
}

pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")

oauth2_scheme = OAuth2PasswordBearer(tokenUrl="/login/token")

app = FastAPI()


def verify_password(plain_password, hashed_password):
    return pwd_context.verify(plain_password, hashed_password)


def get_password_hash(password):
    return pwd_context.hash(password)


def get_user(db, username: str) -> UserInDB:
    if username in db:
        return db[username]


def authenticate_user(fake_db, username: str, password: str):
    user = get_user(fake_db, username)
    if not user:
        return False
    if not verify_password(password, user.hashed_password):
        return False
    return user


def create_access_token(data: dict, expires_delta: timedelta | None = None):
    to_encode = data.copy()
    if expires_delta:
        expire = datetime.utcnow() + expires_delta
    else:
        expire = datetime.utcnow() + timedelta(minutes=15)
    to_encode.update({"exp": expire})
    encoded_jwt = jwt.encode(to_encode, SECRET_KEY, algorithm=ALGORITHM)
    return encoded_jwt


async def get_current_user(token: Annotated[str, Depends(oauth2_scheme)]):
    credentials_exception = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="Could not validate credentials",
        headers={"WWW-Authenticate": "Bearer"},
    )
    try:
        payload = jwt.decode(token, SECRET_KEY, algorithms=[ALGORITHM])
        username: str = payload.get("sub")
        if username is None:
            raise credentials_exception
        token_data = TokenData(username=username)
    except JWTError:
        raise credentials_exception
    user = get_user(fake_users_db, username=token_data.username)
    if user is None:
        raise credentials_exception
    return user


async def get_current_active_user(
        current_user: Annotated[User, Depends(get_current_user)]
):
    if current_user.disabled:
        raise HTTPException(status_code=400, detail="Inactive user")
    return current_user


@app.post("/update_secret", status_code=status.HTTP_200_OK)
async def update_secret():
    global SECRET_KEY
    SECRET_KEY = os.urandom(32)


def make_token(form_data: OAuth2PasswordRequestForm):
    user = authenticate_user(fake_users_db, form_data.username, form_data.password)
    if not user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Incorrect username or password",
            headers={"WWW-Authenticate": "Bearer"},
        )
    access_token_expires = timedelta(minutes=ACCESS_TOKEN_EXPIRE_MINUTES)
    access_token = create_access_token(
        data={"sub": user.username}, expires_delta=access_token_expires
    )
    return access_token


@app.post("/login/token", response_model=Token)
async def login_for_access_token(
        form_data: Annotated[OAuth2PasswordRequestForm, Depends()]
):
    access_token = make_token(form_data)
    return {"access_token": access_token, "token_type": "bearer"}


@app.post("/login/authenticate")
async def authenticate(
        form_data: Annotated[OAuth2ExtendedForm, Depends()]
):
    access_token = make_token(form_data)
    if form_data.remember is None or not form_data.remember:
        set_cookie = f'user_token={access_token}; Path=/; HttpOnly'
    else:
        set_cookie = f'user_token={access_token}; Path=/; Max-Age={ACCESS_TOKEN_EXPIRE_MINUTES * 60}; HttpOnly'

    return Response(
        content=f'{{"path": "{config.redirect_path}"}}',
        headers={'Set-Cookie': set_cookie}
    )


@app.post("/login/logout")
async def logout():
    return Response(
        headers={'Set-Cookie': f'user_token=; Path=/;'}
    )


@app.get("/user", response_model=User)
async def verify_user(
        current_user: Annotated[User, Depends(get_current_active_user)]
):
    return current_user


@app.get("/login")
async def index():
    return FileResponse("frontend/index.html", media_type="text/html; charset=utf-8")


app.mount("/login/", StaticFiles(directory="frontend/"), name="static")
